#!/bin/bash
# mail.sh file.txt field1 field2 field3
# 
# file.txt have to be in the form
# field;...;mail;...;field;...;id;...;field;...;password;...;field;...


for i in `cat $1`
do
        echo "Ligne complète : "$i;
	
	adresse=$(echo  $i|cut -d ";" -f $2)
	echo "Adresse : " $adresse
	
	identifiant=$(echo  $i|cut -d ";" -f $3)
        echo "Identifiant : " $identifiant
	
	password=$(echo  $i|cut -d ";" -f $4)
        echo "Mot de passe : " $password

        grep $adresse fait.txt > /dev/null
        if [ $? -eq 0 ];
        then
                echo $i "Le message a déjà été envoyé"
        else
                echo "Envoi du message vers : " $adresse
                echo "Bonjour."  > message.txt
                echo ""  >> message.txt
                echo "Le système de réservation de ressources est opérationnel." >> message.txt
                echo "Cet outil se nome GRR et il est disponible à l'adresse suivante :"  >> message.txt
                echo "http://www.domaine.com/" >> message.txt
		echo ""  >> message.txt
		echo "Votre identifiant : " $identifiant >> message.txt
		echo "Votre mot de passse : " $password >> message.txt
		echo "" >> message.txt
		echo "Après votre première connexion, vous pourrez changer votre mot de passe (http://www.domain.com/my_account.php)." >> message.txt
		echo "Je suis à votre disposition pour toutes informations complémentaires" >> message.txt
                echo "" >> message.txt
                echo "root@domain.fr" >> message.txt
		echo "PS : Veuillez ne pas tenir compte de mon envoi précédent." >> message.txt

		iconv -f utf8 -t latin1 message.txt   > message2.txt
                cat message2.txt|mail -r "root@domain.fr" -s "Ouverture de l'outil de réservation de salles de réunions" $adresse > /dev/null 2>&1
                sleep  2
                echo $adresse >> fait.txt
        fi
done
